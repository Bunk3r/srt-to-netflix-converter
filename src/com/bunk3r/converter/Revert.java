package com.bunk3r.converter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;

import com.bunk3r.utils.DragNDrop;
import com.bunk3r.utils.FileUtil;
import com.bunk3r.utils.interfaces.DragNDropCallback;

public class Revert implements DragNDropCallback {

	public static void main(String[] args){
		new DragNDrop(new Revert());
	}
	
	public static final String NEW_LINE = System.getProperty("line.separator");

	public void convert(String subtitle, String dir){
		
		/*Opens the File to start writting on it*/
		PrintStream file = null;
		try {
			file = new PrintStream(new FileOutputStream(dir + ".dfxp"));
			System.setOut(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		/*Includes the Header of the File*/
		System.out.print(getHeader());
		
		/*Escapes all the special characters to their xml format*/
		subtitle = StringEscapeUtils.escapeXml(subtitle);
		
		/*Added to capture the last line of the subtitles*/
		subtitle = subtitle + NEW_LINE + NEW_LINE + NEW_LINE;
		
		/*Convert from str to dfxp format*/
		subtitle = subtitle.replaceAll("([0-9]+)[\\s]+([0-9]+:[0-9]+[0-9]+:[0-9]+,[0-9]+)[ ]*--&gt;[ ]*([0-9]+:[0-9]+[0-9]+:[0-9]+,[0-9]+)[\\s]+(([^\n]*\n[^0-9])*)", "\t\t<p begin=\"$2\" end=\"$3\" xml:id=\"subtitle0\">$4</p>");
		
		/*Includes the subtitles in the new file*/
		System.out.print(subtitle);
		
		/*Includes the Footer of the File*/
		System.out.print(getFooter());
		
		/*Closes the File*/
		file.close();
	}

	public String getHeader(){
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>");
		sb.append(NEW_LINE);
		sb.append("<tt xmlns:tt=\"http://www.w3.org/2006/10/ttaf1\" xmlns:ttm=\"http://www.w3.org/2006/10/ttaf1#metadata\" xmlns:ttp=\"http://www.w3.org/2006/10/ttaf1#parameter\" xmlns:tts=\"http://www.w3.org/2006/10/ttaf1#styling\" ttp:tickRate=\"10000000\" xmlns=\"http://www.w3.org/2006/10/ttaf1\">");
		sb.append(NEW_LINE);
		sb.append("<head use=\"dfxp-simplesdh\">");
		sb.append(NEW_LINE);
		sb.append("\t<ttp:profile xmlns:ttp=\"http://www.w3.org/2006/10/ttaf1\"/>");
		sb.append(NEW_LINE);
		sb.append("\t<styling>");
		sb.append(NEW_LINE);
		sb.append("\t\t<style xml:id=\"style0\"/>");
		sb.append(NEW_LINE);
		sb.append("\t\t<style tts:fontStyle=\"italic\" xml:id=\"style1\"/>");
		sb.append(NEW_LINE);
		sb.append("\t</styling>");
		sb.append(NEW_LINE);
		sb.append("</head>");
		sb.append(NEW_LINE);
		sb.append("<body>");
		sb.append(NEW_LINE);
		sb.append("\t<div xml:space=\"preserve\">");
		sb.append(NEW_LINE);
		return sb.toString();
	}
	
	public String getFooter(){
		StringBuilder sb = new StringBuilder();
		sb.append("\t</div>");
		sb.append(NEW_LINE);
		sb.append("</body>");
		sb.append(NEW_LINE);
		sb.append("</tt>");
		return sb.toString();
	}

	/*CallBack to be executed after the files are dragged*/
	public void filesDragged(List<String> files) {
		for(String dir : files){
			String subtitle = FileUtil.readFile(dir);
			convert(subtitle, dir);
		}
	}

}
