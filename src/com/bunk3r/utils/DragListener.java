package com.bunk3r.utils;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.bunk3r.utils.interfaces.DragNDropCallback;

/*
 * EXAMPLE CODE TAKEN FROM
 * blog.christoffer.me
 * 
 * */
public class DragListener implements DropTargetListener {

	private DragNDropCallback callback;

	public void setCallBack(DragNDropCallback callback){
		this.callback = callback;
	}

	public void removeCallBack(DragNDropCallback callback){
		this.callback = null;
	}

	public void drop(DropTargetDropEvent event) {

		// Accept copy drops
		event.acceptDrop(DnDConstants.ACTION_COPY);

		// Get the transfer which can provide the dropped item data
		Transferable transferable = event.getTransferable();

		// Get the data formats of the dropped item
		DataFlavor[] flavors = transferable.getTransferDataFlavors();

		// Loop through the flavors
		for (DataFlavor flavor : flavors) {

			try {

				// If the drop items are files
				if (flavor.isFlavorJavaFileListType()) {

					// Get all of the dropped files
					@SuppressWarnings("unchecked")
					List<File> files = (List<File>) transferable.getTransferData(flavor);
					List<String> filesDirs = new ArrayList<String>();

					// Loop them through
					for (File file : files) {
						filesDirs.add(file.getPath());
					}
					if(null != this.callback){
						callback.filesDragged(filesDirs);
					}
				}

			} catch (Exception e) {
				// Print out the error stack
				e.printStackTrace();
			}
		}

		// Inform that the drop is complete
		event.dropComplete(true);
	}

	public void dragEnter(DropTargetDragEvent event) {
	}

	public void dragExit(DropTargetEvent event) {
	}

	public void dragOver(DropTargetDragEvent event) {
	}

	public void dropActionChanged(DropTargetDragEvent event) {
	}

}