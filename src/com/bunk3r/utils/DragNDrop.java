package com.bunk3r.utils;

import java.awt.BorderLayout;
import java.awt.dnd.DropTarget;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import com.bunk3r.utils.interfaces.DragNDropCallback;

/*
 * EXAMPLE CODE TAKEN FROM
 * blog.christoffer.me
 * 
 * */
public class DragNDrop extends JFrame {

	@Override
	public void dispose() {
		
		super.dispose();
	}

	private static final long serialVersionUID = 1L;

	public DragNDrop(DragNDropCallback callback) {

		// Set the frame title
		super("Drag and drop test");
		
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		// Set the size
		this.setSize(250, 150);

		// Create the label
		JLabel myLabel = new JLabel("Drag something here!", SwingConstants.CENTER);

		// Create the drag and drop listener
		DragListener dragNdropListener = new DragListener();
		dragNdropListener.setCallBack(callback);

		// Connect the label with a drag and drop listener
		new DropTarget(myLabel, dragNdropListener);

		// Add the label to the content
		this.getContentPane().add(BorderLayout.CENTER, myLabel);

		// Show the frame
		this.setVisible(true);

	}

}
