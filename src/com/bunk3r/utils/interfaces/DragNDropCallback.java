package com.bunk3r.utils.interfaces;

import java.util.List;

public interface DragNDropCallback {
	
	public void filesDragged(List<String> files);

}
